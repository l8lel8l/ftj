#include "CameraBehavior.h"
#include "../../Core/Input/InputManager.h"

#include "../../Core/Entity/GameObject.h"

#include "../../Core/FTJ_Console.h"
#include "../../Core/Log/Log.h"

using namespace FTJ;

CameraBehavior::CameraBehavior(FTJ::CGameObject* _go) : FTJ::IBehavior(_go)
{
	OEAddComponent(_go, this);

	CInputManager::GetInstance()->ActivateKey(VK_LBUTTON);
	CInputManager::GetInstance()->ActivateKey(VK_UP);
	CInputManager::GetInstance()->ActivateKey(VK_DOWN);
	CInputManager::GetInstance()->ActivateKey(VK_LEFT);
	CInputManager::GetInstance()->ActivateKey(VK_RIGHT);

	translationMat = XMMatrixTranslationFromVector(m_pTransform->GetPosition());
	rotationMat = XMMatrixIdentity();
	traslacion = XMMatrixIdentity();

	XMMATRIX& m = XMMatrixMultiply(rotationMat, translationMat);
	XMMATRIX& m2 = XMMatrixMultiply(traslacion, translationMat);
	m_pTransform->SetTransform(m);
	m_pTransform->SetTransform(m2);
}

CameraBehavior::~CameraBehavior()
{
}

/*virtual*/ 
void CameraBehavior::Update(const float fDeltaTime)
{
	auto mouseInfo = CInputManager::GetInstance()->GetMouseInput();

	if(true == CInputManager::GetInstance()->GetKeyDown(VK_LBUTTON))
	{
		float deltaX = (float)mouseInfo.x - (float)mouseInfo.prevX;
		float deltaY = (float)mouseInfo.y - (float)mouseInfo.prevY;

		const float MOUSE_SENSITIVITY = 2.f;

		translationMat = XMMatrixTranslationFromVector(m_pTransform->GetPosition());

		if (deltaY != 0 || deltaX != 0)
		{
			if (abs(deltaX) > abs(deltaY))
			{
				XMVECTOR axis = { 0, 1, 0, 1 };

				float angleX = -deltaX * MOUSE_SENSITIVITY* fDeltaTime;
				rotationMat = XMMatrixMultiply(rotationMat, XMMatrixRotationNormal(axis, angleX));
			}
			else if (abs(deltaY) > abs(deltaX))
			{
				XMVECTOR axis = { 1, 0, 0, 1 };

				float angleY = -deltaY * MOUSE_SENSITIVITY* fDeltaTime;
				rotationMat = XMMatrixMultiply(XMMatrixRotationNormal(axis, angleY), rotationMat);
			}

			XMMATRIX& m = XMMatrixMultiply(rotationMat, translationMat);
			m_pTransform->SetTransform(m);
		}
	}
	if (true == CInputManager::GetInstance()->GetKeyPress(VK_UP))
	{
		XMVECTOR aux = XMLoadFloat3(new XMFLOAT3(0, 0, 1));
			traslacion = XMMatrixTranslationFromVector(aux);
			

			XMMATRIX& m = XMMatrixMultiply(traslacion, translationMat);
			m_pTransform->SetTransform(m);
		
	}
	if (true == CInputManager::GetInstance()->GetKeyPress(VK_DOWN))
	{
		XMVECTOR aux = XMLoadFloat3(new XMFLOAT3(0, 0, -1));
		traslacion = XMMatrixTranslationFromVector(aux);


		XMMATRIX& m = XMMatrixMultiply(traslacion, translationMat);
		m_pTransform->SetTransform(m);

	}

	if (true == CInputManager::GetInstance()->GetKeyPress(VK_LEFT))
	{
		XMVECTOR aux = XMLoadFloat3(new XMFLOAT3(-1, 0, 0));
		traslacion = XMMatrixTranslationFromVector(aux);


		XMMATRIX& m = XMMatrixMultiply(traslacion, translationMat);
		m_pTransform->SetTransform(m);

	}
	if (true == CInputManager::GetInstance()->GetKeyPress(VK_RIGHT))
	{
		XMVECTOR aux = XMLoadFloat3(new XMFLOAT3(1, 0, 0));
		traslacion = XMMatrixTranslationFromVector(aux);


		XMMATRIX& m = XMMatrixMultiply(traslacion, translationMat);
		m_pTransform->SetTransform(m);

	}
}
