// ========== TAREA ACADEMICA 1 ==========
// Autor:	Marcos Alexis Sotelo
//
// =======================================
#pragma once
#include "Geometry.h"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <fbxsdk.h>
#include <fbxsdk\core\fbxmanager.h>
#include <fbxsdk\fileio\fbxiosettings.h>
using namespace std;
//
// Imprime la informacion correspondiente al Mesh
// Nota:
//  Utilizar el siguiente formato para esta funcion y los archivos binarios
//  -numero de Vertices
//  -Listar todos los Vertices
//  -numero de Indices
//  -Listar todos los Indices
//
void PrintMesh( fbxMesh& _mesh)
{
	std::cout << "Numero de Vertices: " << _mesh.vertices.size() << endl;
	std::cout << "Formato Vertice( x , y , z): " << endl;
	
	//for (int i = 0; i < _mesh.vertices.size(); i++)
	//{
	//	std::cout << "----> (";
	//	std::cout << _mesh.vertices[i].pos.x << " ,";
	//	std::cout << _mesh.vertices[i].pos.y << " ,";
	//	std::cout << _mesh.vertices[i].pos.z << ")" << endl;
	//}
	std::cout << "------------------------------------------------" << endl;
	std::cout << "Numero de Indices: " << _mesh.indices.size() << endl;
	
	//for (int i = 0; i < _mesh.indices.size(); i++)
	//{
	//	std::cout << _mesh.indices[i] << '\t';
	//}


}

//
// Escribe un archivo binario con la informacion del Mesh
// en la direccion ingresada
//
bool WriteMeshBinary( fbxMesh& _mesh, const char* _filePath)
{
	int numVertices = _mesh.indices.size();
	int numIndices = _mesh.vertices.size();
	fstream archivo;
	archivo.open(_filePath, ios::binary | ios::out);


	if (archivo.is_open()) {
		archivo.write((char*)(&numVertices), sizeof(int));
		for (int i = 0; i < numVertices; i++)
		{
			archivo.write((char*)(&_mesh.vertices[i]), sizeof(FbxMeshVertex));

		}
		archivo.write((char*)(&numIndices), sizeof(int));
		for (int i = 0; i < numIndices; i++)
		{
			archivo.write((char*)(&_mesh.indices[i]), sizeof(int));
		}

		archivo.close();
	}
	else {
		cout << "El archivo no ha podido ser abierto. Verifique el archivo y vuelva a iniciar el programa." << endl;
		
		archivo.close();
		exit(0);
	}
	return true;
}

//
// Lee el archivo binario en la ruta ingresada
// y lo traduce al Mesh enviado
//
bool ReadMeshBinary( fbxMesh* _outMesh, const char* _filePath)
{

	fstream archivo;
	archivo.open(_filePath, ios::binary | ios::in);
	int numVertices;
	int numIndices;
	if (archivo.is_open()) {
		archivo.read((char*)&numVertices, sizeof(int));

		for (int i = 0; i < numVertices; i++)
		{
			FbxMeshVertex tmp;
			archivo.read((char*)&tmp, sizeof(FbxMeshVertex));
			_outMesh->vertices.push_back(tmp);
		}
		archivo.read((char*)&numIndices, sizeof(int));
		for (int i = 0; i < numIndices; i++)
		{
			int tmp;
			archivo.read((char*)&tmp, sizeof(int));
			_outMesh->indices.push_back(tmp);
		}

		archivo.close();
	}
	else {
		cout << "El archivo no ha podido ser abierto." << endl;
		
		archivo.close();
		exit(0);
	}
	return true;
}
void fbx2Obj(string directorio, string nombreFbx) {

	FbxManager *manager = FbxManager::Create();

	FbxScene *scene = FbxScene::Create(manager, "");


	FbxImporter *importer = FbxImporter::Create(manager, "");

	string obj = directorio + nombreFbx + ".fbx";

	importer->Initialize(obj.c_str(), -1);

	importer->Import(scene);

	FbxExporter* exporter = FbxExporter::Create(manager, "");

	string path = directorio + nombreFbx + ".obj";

	exporter->Initialize(path.c_str(), -1);

	exporter->Export(scene);
}

int CrearBinario(string filePath,string nombreArchivo)
{
	 fbxMesh myMesh;

	//std::string filePath = "Assets\\Models\\";

	//nombreArchivo = "dragon"
	fbx2Obj(filePath, nombreArchivo);

	string filenamePath = filePath + nombreArchivo+".obj";

	LoadOBJ(filenamePath.c_str(), &myMesh);

	PrintMesh(myMesh);

	//BINARIO
	std::cout << endl << "===========WriteMeshBinary===========" << endl << endl;

	filenamePath = filePath + nombreArchivo+".bin";

	if (WriteMeshBinary(myMesh, filenamePath.c_str()))
		std::cout << "EXITO: " << filenamePath << " escrito con exito" << endl;
	else
		std::cout << "ERROR: " << filenamePath << " no pudo ser escrito" << endl;

	std::cout << endl << "===========ReadMeshBinary===========" << endl << endl;

	myMesh.vertices.clear();
	myMesh.indices.clear();
	if (ReadMeshBinary(&myMesh, filenamePath.c_str()))
		std::cout << "EXITO: " << filenamePath << " leido con exito" << endl;
	else
		std::cout << "ERROR: " << filenamePath << " no pudo ser leido" << endl;

	PrintMesh(myMesh);

	return 0;
}
